#Installation

## I - Création des répertoires pour les volumes

```shell
mkdir /dockervolumes && (cd /dockervolumes && mkdir -p gitlab/{config,logs,data} && 
mkdir jenkins && mkdir -p postgres/data)
```

## II - Lancement du docker compose

```shell
docker network create pds
```

## III - Installation de la CI via docker

Dans le répertoire `ci` :  

```sh
# Construction des images
docker-compose build

# Lancement du docker compose
docker-compose up -d

# Pour regarder les logs
docker-compose logs -f
```

## IV - Installation de l'application via docker

> Ne comprend que la configuration de postgres pour le moment

Dans le répertoire `app` :

```shell
# Lancement du docker compose
docker-compose up -d
```

